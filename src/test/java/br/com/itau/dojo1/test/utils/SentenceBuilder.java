package br.com.itau.dojo1.test.utils;

import br.com.itau.dojo1.serv.sentence.Sentence;

public class SentenceBuilder {

	private String value = "this is a sentence value";

	private Integer id = 1;

	public Sentence build() {
		final Sentence sentence = new Sentence(value);
		sentence.setId(id);
		return sentence;
	}

	public SentenceBuilder withValue(String value) {
		this.value = value;
		return this;
	}

	public SentenceBuilder withId(Integer id) {
		this.id = id;
		return this;
	}

}
