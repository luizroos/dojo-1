package br.com.itau.dojo1.api.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.itau.dojo1.Application;
import br.com.itau.dojo1.api.sentence.SentenceController;
import br.com.itau.dojo1.serv.sentence.Sentence;
import br.com.itau.dojo1.serv.sentence.SentenceRepository;
import br.com.itau.dojo1.test.utils.SentenceBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class SentenceControllerMockedTest {

	private MockMvc mvc;

	@Mock
	private SentenceRepository sentenceRepository;

	@InjectMocks
	private SentenceController sentenceController;

	@Test
	public void testGetSentences() throws Exception {
		initMocks(this);
		final ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		sentences.add(new SentenceBuilder().withId(1).withValue("teste 1").build());
		sentences.add(new SentenceBuilder().withId(2).withValue("teste 2").build());
		when(sentenceRepository.findAll()).thenReturn(sentences);

		this.mvc = MockMvcBuilders.standaloneSetup(sentenceController).build();
		mvc.perform(get("/sentences")) //
				.andExpect(jsonPath("$", hasSize(2)));
	}

}
