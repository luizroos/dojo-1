package br.com.itau.dojo1.api.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.itau.dojo1.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class SentenceControllerIntegrationTest {

	private MockMvc mvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Test
	public void testGetSentences() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		mvc.perform(get("/sentences")) //
				.andExpect(jsonPath("$", hasSize(4)));
	}

	@Test
	public void testGetLastRepeatedWord() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		mvc.perform(get("/sentences/1/last-repeated-word")) //
				.andExpect(jsonPath("$.value", is("")));
	}

	@Test
	public void testGetLastRepeatedWordNotFound() throws Exception {
		this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		mvc.perform(get("/sentences/9999/last-repeated-word")) //
				.andExpect(status().is(HttpStatus.NOT_FOUND.value()));
	}
}
