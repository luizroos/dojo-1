package br.com.itau.dojo1.serv.sentence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "Sentence")
public class Sentence {

	@Id
	@GeneratedValue
	private Integer id;

	private String value;

	public Sentence() {
	}

	public Sentence(String value) {
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String lastRepeatedWord() {
		// TODO
		return "";
	}

}
