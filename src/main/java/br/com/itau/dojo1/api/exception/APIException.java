package br.com.itau.dojo1.api.exception;

import org.springframework.http.HttpStatus;

public class APIException extends Exception {

	private static final long serialVersionUID = 1L;

	private final HttpStatus status;

	public APIException(HttpStatus status) {
		if (status == null) {
			throw new IllegalArgumentException();
		}
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

}
