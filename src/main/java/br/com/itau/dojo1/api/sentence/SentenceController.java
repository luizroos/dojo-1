package br.com.itau.dojo1.api.sentence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.dojo1.api.SingleAttrGenericJson;
import br.com.itau.dojo1.api.exception.NotFoundAPIException;
import br.com.itau.dojo1.serv.sentence.Sentence;
import br.com.itau.dojo1.serv.sentence.SentenceRepository;

@RestController
public class SentenceController {

	@Autowired
	private SentenceRepository sentenceRepository;

	@RequestMapping(value = "/sentences", //
	method = RequestMethod.GET, //
	produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Sentence> sentences() {
		return sentenceRepository.findAll();
	}

	@RequestMapping(value = "/sentences/{id}/last-repeated-word", //
	method = RequestMethod.GET, //
	produces = MediaType.APPLICATION_JSON_VALUE)
	public SingleAttrGenericJson<String> lastRepeatedWord(@PathVariable("id") Integer id) throws NotFoundAPIException {
		final Sentence sentence = sentenceRepository.findOne(id);
		if (sentence == null) {
			throw new NotFoundAPIException();
		}
		return new SingleAttrGenericJson<String>(sentence.lastRepeatedWord());
	}

}
